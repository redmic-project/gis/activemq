FROM tomcat:8.0-jre8-alpine

ENV GEOSERVER_MAJOR_VERSION="2.12" \
	AMQ_PG_SERVER=localhost \
	AMQ_PG_DATABASE=activemq \
	AMQ_PG_PORT=5432 \
	AMQ_PG_USER=postgres \
	AMQ_PG_PASSWORD=postgres \
	AMQ_PG_DATASOURCE=postgres

ARG TEMP_PATH=/tmp/resources

RUN mkdir -p $TEMP_PATH

RUN apk add --no-cache --virtual .gettext gettext && \
    apk add --no-cache -u libtasn1

# Copy resources
COPY resources $TEMP_PATH


# Install GeoServer Community Plugins
RUN URL="http://ares.boundlessgeo.com/geoserver/${GEOSERVER_MAJOR_VERSION}.x/community-latest" && \
    FILENAME="geoserver-${GEOSERVER_MAJOR_VERSION}-SNAPSHOT-activeMQ-broker-plugin.zip" && \
    if [ ! -f ${TEMP_PATH}/${FILENAME} ]; then \
        curl -L $URL/$FILENAME -o ${TEMP_PATH}/${FILENAME} ; \
    fi; \
    unzip -o ${TEMP_PATH}/${FILENAME} -d ${TEMP_PATH} && \
    # Clean tomcat
    rm -rf ${CATALINA_HOME}/webapps/* && \
    mkdir -p ${CATALINA_HOME}/webapps/activemq && \
    unzip -o ${TEMP_PATH}/activemqBroker-${GEOSERVER_MAJOR_VERSION}-SNAPSHOT.war -d "${CATALINA_HOME}/webapps/activemq" && \
    # Copy Postgresql JAR
    cp $TEMP_PATH/*.jar "${CATALINA_HOME}/webapps/activemq/WEB-INF/lib"

COPY conf/* /usr/local/tomcat/webapps/activemq/WEB-INF/classes/

# Clean
RUN rm -fr ${TEMP_PATH} && \
    rm -rf /var/lib/apt/lists/*

COPY ./scripts /
ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 61666

CMD ["catalina.sh", "run"]