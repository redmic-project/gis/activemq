#!/bin/bash
set -e

FILENAME="standalone-broker"

envsubst < /${FILENAME}.template > ${CATALINA_HOME}/webapps/activemq/WEB-INF/classes/${FILENAME}.properties

exec "$@"